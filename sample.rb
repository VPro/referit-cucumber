require 'airborne'

Airborne.configure do |config|
  config.base_url = "https://echo.getpostman.com"
end

describe "Check status" do
  it "should return 401 status" do
    get "/digest-auth"
    expect_status 401
  end
end
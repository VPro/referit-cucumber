Feature: DigestAuth Request
  As a POSTMAN user
  I want to check API respond
  So I send API request
  
  Scenario: The request should return 401 (Unauthorized) status for the respond
    Given a user configures request
    When the user send API request to the POSTMAN
    Then the respond has status 401 (Unauthorized)
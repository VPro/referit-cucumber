require "airborne"

Given(/^a user configures request$/) do
  Airborne.configure do |config|
    config.base_url = "https://echo.getpostman.com"
  end
end

When(/^the user send API request to the POSTMAN$/) do
  get "/digest-auth"
end

Then(/^the respond has status 401 (Unauthorized)$/) do
  expect_status 401
end